<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'gender', 'age', 'profile_image'];

    protected $hidden = ['password', 'remember_token', 'updated_at'];

    public function social()
    {
        return $this->hasMany('App\SocialUser');
    }

    public function coin()
    {
        return $this->hasOne('App\UserCoin');
    }

    public function statistic()
    {
        return $this->hasOne('App\UserStatistic');
    }

    public function bluff()
    {
        return $this->hasMany('App\UserBluff');
    }

    public function item()
    {
        return $this->hasMany('App\UserItem');
    }

    public function reset_token()
    {
        return $this->hasOne('App\ResetPasswordToken');
    }


}
