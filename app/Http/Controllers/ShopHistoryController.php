<?php

namespace App\Http\Controllers;

use App\ShopHistory;
use Illuminate\Http\Request;

class ShopHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShopHistory  $shopHistory
     * @return \Illuminate\Http\Response
     */
    public function show(ShopHistory $shopHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShopHistory  $shopHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopHistory $shopHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShopHistory  $shopHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShopHistory $shopHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShopHistory  $shopHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopHistory $shopHistory)
    {
        //
    }
}
