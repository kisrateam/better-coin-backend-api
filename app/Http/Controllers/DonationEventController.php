<?php

namespace App\Http\Controllers;

use App\DonationEvent;
use App\Http\JsonResponse;
use Illuminate\Http\Request;
use Validator;

class DonationEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->permission_level < 1)
        {
            return JsonResponse::on_success(DonationEvent::select(
                'name',
                'uri',
                'image',
                'image_cover',
                'description',
                'location',
                'total_donated',
                'received_donated',
                'received_better',
                'max_donated',
                'started_at',
                'ended_at')
                ->where('active', 'active')
                ->paginate());
        }

        return JsonResponse::on_success(DonationEvent::paginate
        ());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'uri' => 'required|string',
            'location' => 'required|string',
            'started_at' => 'required|timestamp',
            'ended_at' => 'required|timestamp'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // create user function
        $this->build($request->all());

        return JsonResponse::on_success();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DonationEvent  $donationEvent
     * @return \Illuminate\Http\Response
     */
    public function show(DonationEvent $donationEvent)
    {
        return JsonResponse::on_success($donationEvent);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DonationEvent  $donationEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(DonationEvent $donationEvent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DonationEvent  $donationEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DonationEvent $donationEvent)
    {
        // create validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'uri' => 'required|string',
            'location' => 'required|string',
            'started_at' => 'required|timestamp',
            'ended_at' => 'required|timestamp'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        $data = $request->all();

        $donationEvent->name = $data["name"];
        $donationEvent->uri = $data["uri"];
        $donationEvent->location = $data["location"];
        $donationEvent->started_at = $data["started_at"];
        $donationEvent->ended_at = $data["ended_at"];
        $donationEvent->active = isset($data["active"])?$data["active"]:0;
        $donationEvent->description = isset($data["description"])?$data["description"]:null;
        $donationEvent->max_donated = isset($data["max_donated"])?$data["max_donated"]:null;

        // check picture
        if (isset($data["image"]))
        {
            // upload picture
            $file_upload = new FileUploadController($donationEvent->id.'.jpg', $data["image"], 'image',  'images/donations');
            // save picture link to user
            $image = $file_upload->url();
            $donationEvent->image = $image;
        }

        // check picture
        if (isset($data["image_cover"]))
        {
            // upload picture
            $file_upload = new FileUploadController($donationEvent->id.'_cover.jpg', $data["image_cover"], 'image',  'images/donations');
            // save picture link to user
            $image_cover = $file_upload->url();
            $donationEvent->image_cover = $image_cover;
        }

        $donationEvent->save();

        return JsonResponse::on_success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DonationEvent  $donationEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(DonationEvent $donationEvent)
    {
        $donationEvent->active = 'inactive';
        $donationEvent->save();
    }

    function build($data)
    {
        $donation_event = new DonationEvent;
        $donation_event->name = $data["name"];
        $donation_event->uri = $data["uri"];
        $donation_event->location = $data["location"];
        $donation_event->started_at = $data["started_at"];
        $donation_event->ended_at = $data["ended_at"];
        $donation_event->active = isset($data["active"])?$data["active"]:0;
        $donation_event->description = isset($data["description"])?$data["description"]:null;
        $donation_event->max_donated = isset($data["max_donated"])?$data["max_donated"]:null;

        $donation_event->save();

        // check picture
        if (isset($data["image"]))
        {
            // upload picture
            $file_upload = new FileUploadController($donation_event->id.'.jpg', $data["image"], 'image',  'images/donations');
            // save picture link to user
            $image = $file_upload->url();
            $donation_event->image = $image;
            $donation_event->save();
        }

        // check picture
        if (isset($data["image_cover"]))
        {
            // upload picture
            $file_upload = new FileUploadController($donation_event->id.'_cover.jpg', $data["image_cover"], 'image',  'images/donations');
            // save picture link to user
            $image_cover = $file_upload->url();
            $donation_event->image_cover = $image_cover;
            $donation_event->save();
        }


    }
}
