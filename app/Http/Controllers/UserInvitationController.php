<?php

namespace App\Http\Controllers;

use App\UserInvitation;
use Illuminate\Http\Request;

class UserInvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserInvitation  $userInvitation
     * @return \Illuminate\Http\Response
     */
    public function show(UserInvitation $userInvitation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserInvitation  $userInvitation
     * @return \Illuminate\Http\Response
     */
    public function edit(UserInvitation $userInvitation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserInvitation  $userInvitation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserInvitation $userInvitation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserInvitation  $userInvitation
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserInvitation $userInvitation)
    {
        //
    }
}
