<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class LineController extends Controller
{

    static $LINE_OAUTH_URI = "https://api.line.me/oauth2/v2.1/token";
    static $LINE_GET_USER = "https://api.line.me/v2/profile";
    static $GRANT_TYPE = "authorization_code";
    static $REDIRECT = "/api/v1/auth/line/oauth";
    static $CLIENT_ID = "1613841815";
    static $CLIENT_SECRET = "8a681c46aa6cc5c69e16e38a857da789";

    public static function oauth(Request $request)
    {
        // success
        if ($request->has('code'))
        {
            $client = new Client();

            $body = [
                'grant_type' => self::$GRANT_TYPE,
                'code' => $request->input('code'),
                'redirect_uri' => url(self::$REDIRECT),
                'client_id' => self::$CLIENT_ID,
                'client_secret' => self::$CLIENT_SECRET
            ];

            try {
                $response = $client->request('POST', self::$LINE_OAUTH_URI,
                    ['verify' => false,
                        'headers' => [
                            "Content-Type" => "application/x-www-form-urlencoded"
                        ],
                        'form_params' => $body
                    ]
                )->getBody()->getContents();

                $access_token = json_decode($response)->access_token;

                $id_token = json_decode($response)->id_token;

                $apy = (object) JWTAuth::getPayload($id_token)->toArray();


                $data_response = [
                    "first_name" => $apy->name,
                    "picture" => $apy->picture,
                    "social_id" => $apy->sub,
                    "social_name" => 'line',
                    "email" => isset($apy->email)?$apy->email:null
                ];

                $social_request = new Request();
                $social_request->merge($data_response);
                $auth = new AuthController();

                return $auth->social_login($social_request);
            }
            catch (BadResponseException $exception)
            {
                return response()->json(["msg" => $exception->getMessage()], 401);
            }

        }

        else if ($request->has('error'))
        {
            return response()->json($request, 401);
        }
    }

    public function access_token(Request $request)
    {
        if ($request->has('access_token')){

            $access_token = $request->input('access_token');

            $client = new Client();

            try
            {
                $request = $client->request("GET", self::$LINE_GET_USER, [
                    'verify' => false,
                    'headers' => [
                        'Authorization' => 'Bearer '.$access_token
                    ]
                ]);

                $response = json_decode($request->getBody()->getContents());

                $picture = base64_encode(file_get_contents($response->pictureUrl));

                $data_response = [
                    "first_name" => $response->displayName,
                    "picture" => $picture,
                    "social_id" => $response->userId,
                    "social_name" => 'line',
                    "email" => isset($response->email)?$response->email:null
                ];

                $social_request = new Request();
                $social_request->merge($data_response);
                $auth = new AuthController();

                return $auth->social_login($social_request);

            }
            catch (BadResponseException $exception)
            {
                return response()->json(["msg" => $exception->getMessage()], 401);
            }
        }
        else {
            return response()->json("Unauthorized", 401);
        }
    }
}
