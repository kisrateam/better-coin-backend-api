<?php

namespace App\Http\Controllers;

use App\UserVerification;
use Illuminate\Http\Request;

class UserVerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserVerification  $userVerification
     * @return \Illuminate\Http\Response
     */
    public function show(UserVerification $userVerification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserVerification  $userVerification
     * @return \Illuminate\Http\Response
     */
    public function edit(UserVerification $userVerification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserVerification  $userVerification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserVerification $userVerification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserVerification  $userVerification
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserVerification $userVerification)
    {
        //
    }
}
