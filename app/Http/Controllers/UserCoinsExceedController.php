<?php

namespace App\Http\Controllers;

use App\UserCoinsExceed;
use Illuminate\Http\Request;

class UserCoinsExceedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserCoinsExceed  $userCoinsExceed
     * @return \Illuminate\Http\Response
     */
    public function show(UserCoinsExceed $userCoinsExceed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserCoinsExceed  $userCoinsExceed
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCoinsExceed $userCoinsExceed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserCoinsExceed  $userCoinsExceed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCoinsExceed $userCoinsExceed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserCoinsExceed  $userCoinsExceed
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCoinsExceed $userCoinsExceed)
    {
        //
    }
}
