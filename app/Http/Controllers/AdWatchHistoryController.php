<?php

namespace App\Http\Controllers;

use App\AdWatchHistory;
use Illuminate\Http\Request;

class AdWatchHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdWatchHistory  $adWatchHistory
     * @return \Illuminate\Http\Response
     */
    public function show(AdWatchHistory $adWatchHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdWatchHistory  $adWatchHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(AdWatchHistory $adWatchHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdWatchHistory  $adWatchHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdWatchHistory $adWatchHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdWatchHistory  $adWatchHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdWatchHistory $adWatchHistory)
    {
        //
    }
}
