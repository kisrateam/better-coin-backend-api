<?php

namespace App\Http\Controllers;

use App\EventQuestHistory;
use Illuminate\Http\Request;

class EventQuestHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventQuestHistory  $eventQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function show(EventQuestHistory $eventQuestHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventQuestHistory  $eventQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(EventQuestHistory $eventQuestHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventQuestHistory  $eventQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventQuestHistory $eventQuestHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventQuestHistory  $eventQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventQuestHistory $eventQuestHistory)
    {
        //
    }
}
