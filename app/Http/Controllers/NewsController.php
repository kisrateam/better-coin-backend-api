<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->permission_level < 1)
        {
            return JsonResponse::on_success(
                News::select('headline', 'uri', 'description', 'image')
                ->where('used', true)
                ->paginate()
            );
        }
        else
        {
            return JsonResponse::on_success(
                News::select('headline', 'uri', 'description', 'image', 'used', 'read_count', 'created_at', 'updated_at')
            ->paginate()
            );
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // create validator
        $validator = Validator::make($request->all(), [
            'headline' => 'required|string',
            'description' => 'required|string',
            'image' => 'required',
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // create user function
        $this->build($request->all());

        return JsonResponse::on_success();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, News $news)
    {
        if ($request->permission_level > 0)
            return JsonResponse::on_success($news);

        if ($news->used)
            return JsonResponse::on_success($news);

        return JsonResponse::on_fails(JsonResponse::$ACCESS_FORBIDDEN);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        // create validator
        $validator = Validator::make($request->all(), [
            'headline' => 'required|string',
            'description' => 'required|string',
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        $data = $request->all();

        $news->headline = $data["name"];
        $news->description = isset($data["description"])?$data["description"]:null;
        $news->used = isset($data["used"])?$data["used"]:0;
        $news->uri = isset($data["uri"])?$data["uri"]:"";

        if (is_null($data["image"])){
            $image = new FileUploadController(uniqid().Carbon::now()->timestamp, $data["image"], $data["image"], "image", "images/news/");
            $news->image = $image->url();
        }

        $news->save();

        return JsonResponse::on_success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();

        return JsonResponse::on_success();
    }

    function build($data)
    {
        // create user
        $news = new News([
            'headline' => $data["headline"],
            'uri' => isset($data["uri"])?$data["uri"]:"",
            'description' => $data["description"],
            'used' => isset($data["used"])?$data["used"]:true,
        ]);

        $image = new FileUploadController(uniqid().Carbon::now()->timestamp, $data["image"], "image", "images/news/");
        $news->image = $image->url();

        $news->save();

        return $news;
    }
}
