<?php

namespace App\Http\Controllers;

use App\LoginDayQuest;
use Illuminate\Http\Request;

class LoginDayQuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoginDayQuest  $loginDayQuest
     * @return \Illuminate\Http\Response
     */
    public function show(LoginDayQuest $loginDayQuest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoginDayQuest  $loginDayQuest
     * @return \Illuminate\Http\Response
     */
    public function edit(LoginDayQuest $loginDayQuest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoginDayQuest  $loginDayQuest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoginDayQuest $loginDayQuest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoginDayQuest  $loginDayQuest
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoginDayQuest $loginDayQuest)
    {
        //
    }
}
