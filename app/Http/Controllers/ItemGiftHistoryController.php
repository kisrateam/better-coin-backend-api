<?php

namespace App\Http\Controllers;

use App\ItemGiftHistory;
use Illuminate\Http\Request;

class ItemGiftHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemGiftHistory  $itemGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function show(ItemGiftHistory $itemGiftHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemGiftHistory  $itemGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemGiftHistory $itemGiftHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemGiftHistory  $itemGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemGiftHistory $itemGiftHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemGiftHistory  $itemGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemGiftHistory $itemGiftHistory)
    {
        //
    }
}
