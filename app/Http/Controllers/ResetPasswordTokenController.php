<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\Notifications\ResetPasswordRequest;
use App\Notifications\ResetPasswordSuccess;
use App\ResetPasswordToken;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class ResetPasswordTokenController extends Controller
{

    protected $expired_minute = 720;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), ['email' => 'required|string|email',]);

        if ($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        $user = User::where('email', $request->input('email'))->first();

        if (!$user)
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);

        $password_reset = ResetPasswordToken::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
             ]
        );

        if ($user && $password_reset)
            $user->notify(
                new ResetPasswordRequest($password_reset->token)
            );

        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ResetPasswordToken  $resetPasswordToken
     * @return \Illuminate\Http\Response
     */
    public function show(ResetPasswordToken $resetPasswordToken)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ResetPasswordToken  $resetPasswordToken
     * @return \Illuminate\Http\Response
     */
    public function edit(ResetPasswordToken $resetPasswordToken)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ResetPasswordToken  $resetPasswordToken
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ResetPasswordToken $resetPasswordToken)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ResetPasswordToken  $resetPasswordToken
     * @return \Illuminate\Http\Response
     */
    public function destroy(ResetPasswordToken $resetPasswordToken)
    {
        //
    }

    public function find($token)
    {
        $password_reset = ResetPasswordToken::where('token', $token)
            ->first();

        if (is_null($password_reset))
            return JsonResponse::on_fails(JsonResponse::$NOT_FOUND, 'This password reset token is invalid.');

        if (Carbon::parse($password_reset->updated_at)->addMinutes($this->expired_minute)->isPast()) {
            $password_reset->delete();
            return JsonResponse::on_fails(JsonResponse::$NOT_FOUND, 'This password reset token is invalid.');
        }

        return JsonResponse::on_success($password_reset);
    }

    public function reset(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        if ($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        $password_reset = ResetPasswordToken::where([
            ['token', $request->input('token')],
            ['email', $request->input('email')]
        ])->first();

        if (is_null($password_reset))
            return JsonResponse::on_fails(JsonResponse::$NOT_FOUND, 'This password reset token is invalid.');

        $user = User::where('email', $password_reset->email)->first();

        if (is_null($user))
            return JsonResponse::on_fails(JsonResponse::$NOT_FOUND, 'We can\'t find a user with that e-mail address.');

        $user->password = bcrypt($request->input('password'));

        $user->save();
        $password_reset->delete();

        $user->notify(new ResetPasswordSuccess($password_reset));

        return JsonResponse::on_success($user);
    }
}
