<?php

namespace App\Http\Controllers;

use App\OTPSms;
use Illuminate\Http\Request;

class OTPSmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OTPSms  $oTPSms
     * @return \Illuminate\Http\Response
     */
    public function show(OTPSms $oTPSms)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OTPSms  $oTPSms
     * @return \Illuminate\Http\Response
     */
    public function edit(OTPSms $oTPSms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OTPSms  $oTPSms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OTPSms $oTPSms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OTPSms  $oTPSms
     * @return \Illuminate\Http\Response
     */
    public function destroy(OTPSms $oTPSms)
    {
        //
    }
}
