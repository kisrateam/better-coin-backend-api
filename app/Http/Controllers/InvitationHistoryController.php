<?php

namespace App\Http\Controllers;

use App\InvitationHistory;
use Illuminate\Http\Request;

class InvitationHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvitationHistory  $invitationHistory
     * @return \Illuminate\Http\Response
     */
    public function show(InvitationHistory $invitationHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvitationHistory  $invitationHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(InvitationHistory $invitationHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvitationHistory  $invitationHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvitationHistory $invitationHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvitationHistory  $invitationHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvitationHistory $invitationHistory)
    {
        //
    }
}
