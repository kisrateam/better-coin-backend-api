<?php

namespace App\Http\Controllers;

use App\WeeklyRankingHistory;
use Illuminate\Http\Request;

class WeeklyRankingHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeeklyRankingHistory  $weeklyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function show(WeeklyRankingHistory $weeklyRankingHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeeklyRankingHistory  $weeklyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(WeeklyRankingHistory $weeklyRankingHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeeklyRankingHistory  $weeklyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeeklyRankingHistory $weeklyRankingHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeeklyRankingHistory  $weeklyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeeklyRankingHistory $weeklyRankingHistory)
    {
        //
    }
}
