<?php

namespace App\Http\Controllers;

use App\EventQuest;
use Illuminate\Http\Request;

class EventQuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventQuest  $eventQuest
     * @return \Illuminate\Http\Response
     */
    public function show(EventQuest $eventQuest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventQuest  $eventQuest
     * @return \Illuminate\Http\Response
     */
    public function edit(EventQuest $eventQuest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventQuest  $eventQuest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventQuest $eventQuest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventQuest  $eventQuest
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventQuest $eventQuest)
    {
        //
    }
}
