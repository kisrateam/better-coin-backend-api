<?php

namespace App\Http\Controllers;

use App\SocialUser;
use Illuminate\Http\Request;

class SocialUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SocialUser  $socialUser
     * @return \Illuminate\Http\Response
     */
    public function show(SocialUser $socialUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SocialUser  $socialUser
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialUser $socialUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SocialUser  $socialUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialUser $socialUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SocialUser  $socialUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialUser $socialUser)
    {
        //
    }

    public static function build($data)
    {
        $social_user = new SocialUser([
            'social_name' => $data["social_name"],
            'social_id' => $data["social_id"],
            'user_id' => $data["user_id"]
        ]);

        $social_user->save();

        return $social_user;
    }
}
