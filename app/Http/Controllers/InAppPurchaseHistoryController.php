<?php

namespace App\Http\Controllers;

use App\InAppPurchaseHistory;
use Illuminate\Http\Request;

class InAppPurchaseHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InAppPurchaseHistory  $inAppPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function show(InAppPurchaseHistory $inAppPurchaseHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InAppPurchaseHistory  $inAppPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(InAppPurchaseHistory $inAppPurchaseHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InAppPurchaseHistory  $inAppPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InAppPurchaseHistory $inAppPurchaseHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InAppPurchaseHistory  $inAppPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(InAppPurchaseHistory $inAppPurchaseHistory)
    {
        //
    }
}
