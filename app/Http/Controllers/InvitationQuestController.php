<?php

namespace App\Http\Controllers;

use App\InvitationQuest;
use Illuminate\Http\Request;

class InvitationQuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvitationQuest  $invitationQuest
     * @return \Illuminate\Http\Response
     */
    public function show(InvitationQuest $invitationQuest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvitationQuest  $invitationQuest
     * @return \Illuminate\Http\Response
     */
    public function edit(InvitationQuest $invitationQuest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvitationQuest  $invitationQuest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvitationQuest $invitationQuest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvitationQuest  $invitationQuest
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvitationQuest $invitationQuest)
    {
        //
    }
}
