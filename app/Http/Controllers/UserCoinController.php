<?php

namespace App\Http\Controllers;

use App\UserCoin;
use Illuminate\Http\Request;

class UserCoinController extends Controller
{

    static $AMOUNT_INIT = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserCoin  $userCoin
     * @return \Illuminate\Http\Response
     */
    public function show(UserCoin $userCoin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserCoin  $userCoin
     * @return \Illuminate\Http\Response
     */
    public function edit(UserCoin $userCoin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserCoin  $userCoin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserCoin $userCoin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserCoin  $userCoin
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserCoin $userCoin)
    {
        //
    }

    public static function build($data)
    {
        $user_coin = new UserCoin([
           "user_id" => $data["user_id"],
           "better_amount" => self::$AMOUNT_INIT
        ]);

        $user_coin->save();

        return $user_coin;
    }
}
