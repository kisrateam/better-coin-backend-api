<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\SystemConfig;
use Illuminate\Http\Request;
use Validator;

class SystemConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create validator
        $validator = Validator::make($request->all(), [
            'step_exceed' => 'required|integer',
            'ad_watch_exceed' => 'required|integer',
            'user_item_capacity' => 'required|integer',
            'better_rate' => 'required|integer',
            'friend_coin_limit' => 'required|integer',
            'friend_coin_amount' => 'required',
            'used' => 'required|integer'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // create user function
        $this->build($request->all());

        return JsonResponse::on_success();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function show(SystemConfig $systemConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(SystemConfig $systemConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SystemConfig $systemConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SystemConfig  $systemConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(SystemConfig $systemConfig)
    {
        //
    }

    public function build($data)
    {
        $system_config = new SystemConfig($data);
        $system_config->save();

        return $system_config;
    }

    public static function walk_ratio()
    {
        $better_rate = SystemConfig::where('used', true)->orderBy('created_at', 'desc')->first();

        if (is_null($better_rate))
            $better_rate = SystemConfig::$DEFAULT_BETTER_RATE;

        return $better_rate/100;

    }
}
