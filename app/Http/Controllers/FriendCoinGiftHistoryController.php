<?php

namespace App\Http\Controllers;

use App\FriendCoinGiftHistory;
use Illuminate\Http\Request;

class FriendCoinGiftHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FriendCoinGiftHistory  $friendCoinGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function show(FriendCoinGiftHistory $friendCoinGiftHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FriendCoinGiftHistory  $friendCoinGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(FriendCoinGiftHistory $friendCoinGiftHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FriendCoinGiftHistory  $friendCoinGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FriendCoinGiftHistory $friendCoinGiftHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FriendCoinGiftHistory  $friendCoinGiftHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(FriendCoinGiftHistory $friendCoinGiftHistory)
    {
        //
    }
}
