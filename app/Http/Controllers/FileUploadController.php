<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Support\Facades\Storage;

class FileUploadController extends Controller
{
    public function __construct($filename, $file, $file_type = "file", $path = "upload")
    {
        $this->filename = $filename;
        $this->file = $file;
        $this->path = $path;

        switch ($file_type){
            case "file":
                $this->upload_file();
                break;
            case "image":
                $this->upload_image();
        }

    }

    function upload_file()
    {
        // check has folder if folder does not exist will create
        if (!file_exists(public_path($this->path)))  File::makeDirectory(public_path($this->path), 0775, true);

        $path = $this->path.'/'.$this->filename;
        File::put(public_path($path), $this->file);

        $this->file_url =  url($path);
    }

    function upload_image() {

        // check unused base64 string 'data:image base64' in the first of string will delete
        $check_unused_word = preg_replace('#^data:image/\w+;base64,#i', '', $this->file);

        //decode to base64
        $decode_base64 = base64_decode($check_unused_word);

        // check has folder if folder does not exist will create
        if (!file_exists(public_path($this->path)))  File::makeDirectory(public_path($this->path), 0775, true);

        $path = $this->path.'/'.$this->filename;
        File::put(public_path($path), $decode_base64);

        $this->file_url = url($path);
    }

    public function url()
    {
        return $this->file_url;
    }



}
