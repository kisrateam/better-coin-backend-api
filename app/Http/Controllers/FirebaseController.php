<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{


    static protected $DATABASE = "https://better-coin.firebaseio.com/";

    protected $firebase;

    protected $database;

    public function __construct()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(storage_path('app/better-coin-firebase-adminsdk-px6nq-c590ace1e2.json'));

        $this->firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://better-coin.firebaseio.com/')
            ->create();

        $this->database = $this->firebase->getDatabase();

    }

    public function push($ref, $data)
    {
        $ref =  $this->database->getReference($ref);
        $ref->push($data);
    }


    public function get($ref)
    {
        $ref =  $this->database->getReference($ref);
        return $ref->getValue();
    }

    public function walk(Request $request)
    {
        $this->push('test', ($request->all()));

        return JsonResponse::on_success($request->all());
    }

    public function test(Request $request)
    {
        $data = ["id" => 1,
            "location" => '100.00,1.2',
            "walk" => 12];
        $this->push($request->user()->id."/step", $data);

        return response()->json('sss');
    }

    public function coin_variation(Request $request)
    {
        $coin_ratio = SystemConfigController::walk_ratio();

        $event_location = null;


        return JsonResponse::on_success([
            "coin_ratio" => $coin_ratio,

            // go on
            ]);
    }

}
