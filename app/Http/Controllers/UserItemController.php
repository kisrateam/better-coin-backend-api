<?php

namespace App\Http\Controllers;


use App\UserItem;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserItem  $userItem
     * @return \Illuminate\Http\Response
     */
    public function show(UserItem $userItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserItem  $userItem
     * @return \Illuminate\Http\Response
     */
    public function edit(UserItem $userItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserItem  $userItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserItem $userItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserItem  $userItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserItem $userItem)
    {
        //
    }

    public static function build($data)
    {
        $user_item = new UserItem([
            "user_id" => $data["user_id"],
            "item_id" => $data["item_id"],
        ]);

        $user_item->save();

        return $user_item;
    }

    public static function use(UserItem $user_item)
    {
        // TODO:: will update on history
        $time_used = Carbon::now();

        $user_item->used = true;
        $user_item->used_at = $time_used;

        $user_item->save();
    }

}
