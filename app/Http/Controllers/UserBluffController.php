<?php

namespace App\Http\Controllers;

use App\UserBluff;
use Illuminate\Http\Request;

class UserBluffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserBluff  $userBluff
     * @return \Illuminate\Http\Response
     */
    public function show(UserBluff $userBluff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserBluff  $userBluff
     * @return \Illuminate\Http\Response
     */
    public function edit(UserBluff $userBluff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserBluff  $userBluff
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserBluff $userBluff)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserBluff  $userBluff
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserBluff $userBluff)
    {
        //
    }
}
