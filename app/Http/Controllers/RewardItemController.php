<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\RewardItem;
use Illuminate\Http\Request;

class RewardItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // create validator
        $validator = Validator::make($request->all(), [
            'reward_set_id' => 'required|integer',
            'item_id' => 'required|integer',
            'reward_type' => 'required',
            'amount' => 'required'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // create user function
        $this->build($request->all(), $request->input('reward_set_id'));

        return JsonResponse::on_success();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RewardItem  $rewardItem
     * @return \Illuminate\Http\Response
     */
    public function show(RewardItem $rewardItem)
    {
        $reward_item["reward_set"] = $rewardItem->reward_set;
        $reward_item["reward_type"] = $rewardItem->reward_type;

        if($rewardItem->reward_type === 'coin')
        {
            $reward_item["coin"] = $rewardItem->amount;
        }
        else
        {
            $reward_item["item"] = $rewardItem->item;
            $reward_item["amount"] = $rewardItem->amount;
        }

        return JsonResponse::on_success($reward_item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RewardItem  $rewardItem
     * @return \Illuminate\Http\Response
     */
    public function edit(RewardItem $rewardItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RewardItem  $rewardItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RewardItem $rewardItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RewardItem  $rewardItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(RewardItem $rewardItem)
    {
        try
        {
            $rewardItem->delete();
        }
        catch (\Exception $exception)
        {
            return JsonResponse::on_fails(JsonResponse::$ERROR, $exception->getMessage());
        }

        return JsonResponse::on_success();
    }

    public static function get($reward_set_id = null)
    {
        if (!is_null($reward_set_id))
            $reward_items = RewardItem::where('reward_set_id', $reward_set_id)->get();
        else
            $reward_items = RewardItem::get();

        return $reward_items;
    }

    public static function build($data, $reward_set_id)
    {
        try
        {
            $reward_items = [];
            foreach ($data as $datum)
            {
                $reward_item = new RewardItem([
                    'reward_set_id' => $reward_set_id,
                    'item_id' => $datum->item_id,
                    'reward_type' => $datum->reward_type,
                    'amount' => $datum->amount
                ]);

                $reward_item->save();
                $reward_items[] = $reward_item;

            }
            return $reward_items;
        }
        catch (\Exception $exception)
        {
            return $exception->getMessage();
        }
    }

}
