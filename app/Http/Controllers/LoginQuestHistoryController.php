<?php

namespace App\Http\Controllers;

use App\LoginQuestHistory;
use Illuminate\Http\Request;

class LoginQuestHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoginQuestHistory  $loginQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function show(LoginQuestHistory $loginQuestHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoginQuestHistory  $loginQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(LoginQuestHistory $loginQuestHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoginQuestHistory  $loginQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoginQuestHistory $loginQuestHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoginQuestHistory  $loginQuestHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoginQuestHistory $loginQuestHistory)
    {
        //
    }
}
