<?php

namespace App\Http\Controllers;

use App\MonthlyRankingHistory;
use Illuminate\Http\Request;

class MonthlyRankingHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MonthlyRankingHistory  $monthlyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function show(MonthlyRankingHistory $monthlyRankingHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MonthlyRankingHistory  $monthlyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(MonthlyRankingHistory $monthlyRankingHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MonthlyRankingHistory  $monthlyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MonthlyRankingHistory $monthlyRankingHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MonthlyRankingHistory  $monthlyRankingHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MonthlyRankingHistory $monthlyRankingHistory)
    {
        //
    }
}
