<?php

namespace App\Http\Controllers;

use App\AdWatch;
use Illuminate\Http\Request;

class AdWatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdWatch  $adWatch
     * @return \Illuminate\Http\Response
     */
    public function show(AdWatch $adWatch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdWatch  $adWatch
     * @return \Illuminate\Http\Response
     */
    public function edit(AdWatch $adWatch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdWatch  $adWatch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdWatch $adWatch)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdWatch  $adWatch
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdWatch $adWatch)
    {
        //
    }
}
