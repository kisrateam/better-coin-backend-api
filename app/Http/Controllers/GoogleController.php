<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function oauth(Request $request)
    {

        if ($request->has('code'))
        {
            $code = $request->input('code');
            $access = Socialite::driver('google')->getAccessTokenResponse($code);

            try
            {
                $access_token = $access['access_token'];

                return $this->get_profile($access_token);
            }
            catch (\Exception $exception)
            {
                return response()->json($exception->getMessage(), 401);
            }

        }
        else
        {

        }

    }
    function access_token(Request $request)
    {
        if ($request->has('access_token'))
        {
            $access_token = $request->input('access_token');

            return $this->get_profile($access_token);
        }
        else
        {
            return response()->json('Unauthorized', 401);
        }
    }

    function get_profile($access_token)
    {
        try
        {
            $user = Socialite::driver('google')->userFromToken($access_token);
        }
        catch (\Exception $exception)
        {
            return response()->json($exception->getMessage(), 401);
        }

        $social_id = $user->id;
        $email = $user->email;
        $picture = base64_encode(file_get_contents($user->avatar_original));
        $last_name = '';

        if (strlen($user->name) === 0)
        {
            if (is_null($user->nickname))
            {
                $name_from_email = explode('@',$email);
                $first_name = $name_from_email[0];
            }
            else
            {
                $first_name = $user->nickname;
            }
        }
        else
        {
            $name = explode(' ', $user->name);
            $first_name = $name[0];
            if(is_null($user->nickname))
                $last_name = $name[1];
            else
                $last_name = $name[2];
        }


        $data_response = [
            "first_name" => $first_name,
            "last_name" => $last_name,
            "picture" => $picture,
            "social_id" => $social_id,
            "social_name" => 'google',
            "email" => $email
        ];

        $social_request = new Request();
        $social_request->merge($data_response);
        $auth = new AuthController();

        return $auth->social_login($social_request);
    }
}
