<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\MainQuest;
use Illuminate\Http\Request;

class MainQuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create validator
        $validator = Validator::make($request->all(), [
            'quest_type' => 'required|string',
            'description' => 'string',
            'image' => 'required'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());


        return JsonResponse::on_success($this->build($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MainQuest  $mainQuest
     * @return \Illuminate\Http\Response
     */
    public function show(MainQuest $mainQuest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MainQuest  $mainQuest
     * @return \Illuminate\Http\Response
     */
    public function edit(MainQuest $mainQuest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MainQuest  $mainQuest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MainQuest $mainQuest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MainQuest  $mainQuest
     * @return \Illuminate\Http\Response
     */
    public function destroy(MainQuest $mainQuest)
    {
        //
    }

    public function build($data)
    {

    }
}
