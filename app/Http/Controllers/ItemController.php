<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\Item;
use Illuminate\Http\Request;
use Validator;

class ItemController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->permission_level < 1)
        {
            return JsonResponse::on_success(Item::select(
                'id',
                'name',
                'description',
                'image',
                'coin',
                'ability_period',
                'ability_capacity',
                'ability_multiple',
                'better_price',
                'money_price',
                'duration')
                ->where('active', 'active')
                ->paginate());
        }

        return JsonResponse::on_success(Item::paginate
        ());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // create validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'string',
            'image' => 'required'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // create user function
        $this->build($request->all());

        return JsonResponse::on_success();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return JsonResponse::on_success($item);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {

        // create validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'string',
            'image' => 'required'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        $data = $request->all();


        $item->name = $data["name"];
        $item->description = isset($data["description"])?$data["description"]:null;
        $item->coin = isset($data["coin"])?$data["coin"]:0;
        $item->ability_period = isset($data["ability_period"])?$data["ability_period"]:0;
        $item->ability_capacity = isset($data["ability_capacity"])?$data["ability_capacity"]:0;
        $item->ability_multiple = isset($data["ability_multiple"])?$data["ability_multiple"]:0;
        $item->better_price = isset($data["better_price"])?$data["better_price"]:null;
        $item->money_price = isset($data["money_price"])?$data["money_price"]:null;
        $item->duration = isset($data["duration"])?$data["duration"]:null;

        if (is_null($data["image"])){
            $image = new FileUploadController($data["name"].uniqid(), $data["image"], "image", "images/items/");
            $item->image = $image->url();
        }

        $item->save();

        return JsonResponse::on_success();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Item $item)
    {
        $item->active = 'inactive';
        $item->save();


    }

    function build($data)
    {
        // create user
        $item = new Item([
            'name' => $data["name"],
            'ability' => isset($data["ability"])?$data["ability"]:null,
            'coin' => isset($data["coin"])?$data["coin"]:0,
            'ability_period' => isset($data["ability_period"])?$data["ability_period"]:0,
            'ability_capacity' => isset($data["ability_capacity"])?$data["ability_capacity"]:0,
            'ability_multiple' => isset($data["ability_multiple"])?$data["ability_multiple"]:0,
            'better_price' => isset($data["better_price"])?$data["better_price"]:null,
            'money_price' => isset($data["money_price"])?$data["money_price"]:null,
            'duration' => isset($data["duration"])?$data["duration"]:null,
        ]);

        $filename = str_replace(" ", "-", $data["name"]).uniqid();

        $image = new FileUploadController($filename, $data["image"], "image", "images/items");
        $item->image = $image->url();

        $item->save();

        return $item;
    }
}
