<?php

namespace App\Http\Controllers;

use App\BetterPurchaseHistory;
use Illuminate\Http\Request;

class BetterPurchaseHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BetterPurchaseHistory  $betterPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function show(BetterPurchaseHistory $betterPurchaseHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BetterPurchaseHistory  $betterPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(BetterPurchaseHistory $betterPurchaseHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BetterPurchaseHistory  $betterPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BetterPurchaseHistory $betterPurchaseHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BetterPurchaseHistory  $betterPurchaseHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BetterPurchaseHistory $betterPurchaseHistory)
    {
        //
    }
}
