<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Validator, File;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->permission_level <0)
        {
            return  JsonResponse::on_success(User::where('active', 'active')
                ->paginate());
        }
        else
        {
            return JsonResponse::on_success(User::paginate());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // view of create
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // create validator
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'gender' => 'required|string',
            'age' => 'required|integer',
            'profile_image' => 'required|string',
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // create user function
        $this->build($request->all());

        return JsonResponse::on_success();

    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
            return JsonResponse::on_success($user);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // view of edit
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        // make input validator
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|email',
            'gender' => 'required|string',
            'age' => 'required|integer'
        ]);

        // check validator fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // check is admin or this user for permission
        if ($user->id === $request->user()->id || $request->permission_level > 0)
        {
                // check password and confirm passowrd
                if ($request->input('password') === $request->input('password_confirmation'))
                {
                    // edit user from input
                    $user->first_name = $request->input('first_name');
                    $user->last_name = $request->input('last_name');
                    $user->email = $request->input('email');
                    $user->password = strlen($request->input('password')) !== 0?bcrypt($request->input('password')): $user->password;
                    $user->gender = $request->input('gender');
                    $user->age = $request->input('age');

                    if ($request->input("permission_level") > 0)
                        $user->group = $request->input("group");

                    try
                    {
                        $user->save();
                    }
                    catch (QueryException $e)
                    {
                        $error_code = $e->errorInfo[1];

                        // email exist on edit user
                        if($error_code === 1062){
                            return JsonResponse::on_fails(JsonResponse::$DATA_EXIST, $validator->errors());
                        }
                        else
                        {
                            return JsonResponse::on_fails(JsonResponse::$ERROR, $e->getMessage());
                        }
                    }

                    return JsonResponse::on_success($user);
                }
                else
                    return JsonResponse::on_fails(JsonResponse::$AUTHORIZED, 'new password confirmation failed.');
        }
        else
            return JsonResponse::on_fails(JsonResponse::$ACCESS_FORBIDDEN);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        // check user admin permission or this user permission
        if ($user->id === $request->user()->id || $request->permission_level > 0)
        {
            $user->active = 'inactive';
            $user->save();

            return JsonResponse::on_success();
        }
        else
        {
            return JsonResponse::on_fails(JsonResponse::$ACCESS_FORBIDDEN);
        }
    }

    public function upload_profile_image(Request $request)
    {
        // check input
        if ($request->has('profile_image'))
        {
            // upload image
            $file_upload = new FileUploadController($request->user()->id.'.jpg', $request->input('profile_image'), 'image',  'images/profile');

            // save link to user
            $request->user()->profile_image = $file_upload->url();
            $request->user()->save();
            return JsonResponse::on_success();
        }
        else
        {
            return JsonResponse::on_fails(JsonResponse::$MALFORMED);
        }
    }

    public function remove_profile_image(Request $request)
    {
        try
        {
            // delete image
            $file = storage_path('/images/profile/'.$request->user()->id.'jpg');
            File::delete($file);

            // save null to user
            $request->user()->profile_image = null;
            $request->user()->save();

            return JsonResponse::on_success();
        }
        catch (\Exception $exception)
        {
            return JsonResponse::on_fails(JsonResponse::$ERROR, $exception->getMessage());
        }
    }

    public function coin(Request $request)
    {
        return JsonResponse::on_success($request->user()->coin);
    }

    public function statistic(Request $request)
    {
        return JsonResponse::on_success($request->user()->statistic);
    }

    public function bluff(Request $request)
    {
        // get bluff
        $bluffs = $request->user()->bluff;

        // create active bluff
        $active_bluff = [];

        foreach ($bluffs as $bluff)
        {
            // check expired bluff
            if ($bluff->ended_at)
            {
                $bluff->active = 0;
                $bluff->save();

            }
            // check active bluff
            if ($bluff->active)
            {
                $active_bluff[] = $bluff;
            }
        }

        return JsonResponse::on_success($active_bluff);
    }

    public function item(Request $request)
    {
        $data = [];
        foreach ($request->user()->item as $item)
        {
            $item_data = $item;
            $item_data->detail = $item->detail;

            $data[] = $item_data;

        }
        return JsonResponse::on_success($data);

    }

    public static function build($data)
    {

        // create user
        $user = new User([
            'first_name' => $data["first_name"],
            'last_name' => isset($data["last_name"])?$data["last_name"]:null,
            'email' => $data["email"],
            'password' => !is_null($data["password"])?bcrypt($data["password"]): null,
            'gender' => isset($data["gender"])?$data["gender"]:null,
            'age' => isset($data["age"])?$data["age"]:null,
            'profile_image' => null,
            'verified' => false,
            'active' => true
        ]);

        if (isset($data["permission_level"]))
        {
            if ($data["permission_level"] > 0)
                $user->group = $data["group"];
        }


        $user->save();

        // check picture
        if (isset($data["picture"]))
        {
            // upload picture
            $file_upload = new FileUploadController($user->id.'.jpg', $data["picture"], 'image',  'images/profile');
            // save picture link to user
            $picture = $file_upload->url();
            $user->profile_image = $picture;
            $user->save();
        }

        // check register from social
        if (isset($data->social_name)) {
            $data["user_id"] = $user->id;
            // create social user
            SocialUserController::build($data);
        }

        // create user coin
        UserCoinController::build([
           "user_id" => $user->id
        ]);

        // create user statistic
        UserStatisticController::build([
            "user_id" => $user->id
        ]);

        return $user;
    }



}
