<?php

namespace App\Http\Controllers;

use App\UserItemUsedHistory;
use Illuminate\Http\Request;

class UserItemUsedHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserItemUsedHistory  $userItemUsedHistory
     * @return \Illuminate\Http\Response
     */
    public function show(UserItemUsedHistory $userItemUsedHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserItemUsedHistory  $userItemUsedHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(UserItemUsedHistory $userItemUsedHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserItemUsedHistory  $userItemUsedHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserItemUsedHistory $userItemUsedHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserItemUsedHistory  $userItemUsedHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserItemUsedHistory $userItemUsedHistory)
    {
        //
    }
}
