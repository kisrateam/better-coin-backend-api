<?php

namespace App\Http\Controllers;

use App\UserStatistic;
use Illuminate\Http\Request;

class UserStatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserStatistic  $userStatistic
     * @return \Illuminate\Http\Response
     */
    public function show(UserStatistic $userStatistic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserStatistic  $userStatistic
     * @return \Illuminate\Http\Response
     */
    public function edit(UserStatistic $userStatistic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserStatistic  $userStatistic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserStatistic $userStatistic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserStatistic  $userStatistic
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserStatistic $userStatistic)
    {
        //
    }

    public static function build($data)
    {
        $user_statistic = new UserStatistic([
         "user_id" => $data["user_id"]
        ]);

        $user_statistic->save();

        return $user_statistic;
    }
}
