<?php

namespace App\Http\Controllers;
use App\Http\JsonResponse;
use App\SocialUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Validator;

class AuthController extends Controller
{

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials))
            return JsonResponse::on_fails(JsonResponse::$AUTHORIZED);

        $user = $request->user();
        $response = $this->initial_user($user);

        return $response;
    }

    public function backend_login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        $credentials = request(['email', 'password']);

        $user = User::where('email', $request->input('email'))->where('group', '!=', 'user')->first;
        if (is_null($user))
            return JsonResponse::on_fails(JsonResponse::$AUTHORIZED);

        if(!Auth::attempt($credentials))
            return JsonResponse::on_fails(JsonResponse::$AUTHORIZED);

        $user = $request->user();
        $response = $this->initial_user($user);

        return $response;
    }

    public function social_login(Request $request) {

        $validator = Validator::make($request->all(), [
            'social_name' => 'required|string',
            'social_id' => 'required',
        ]);

        if ($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED);

        $social_user = SocialUser::where('social_name', $request->input('social_name'))->where('social_id', $request->input('social_id'))->where('active', true)->first();

        if (!is_null($social_user))
        {
            $user = $social_user->user;

        }
        else {
            $validator = Validator::make($request->all(), [
                "email"=> 'required|email|unique:users'
            ]);

            if ($validator->passes()) {

                $data = $request->all();

                if (!is_null($data["email"]) && strlen($data["email"]) === 0)
                {
                    $data["email"] = null;
                }

                $data["password"] = null;

                $user = $this->signup_user($data);
            }
            else {
                $user = User::where('email', $request->input('email'))->first();
            }

            SocialUserController::build([
                "social_name" => $request->input('social_name'),
                "social_id" => $request->input('social_id'),
                "user_id" => $user->id
            ]);

        }

        Auth::login($user);

        $response = $this->initial_user($user);

        return $response;

    }

    public function signup_user($data) {
       $user = UserController::build($data);
        return $user;
    }

    public function initial_user($user)
    {
        $user->last_login = Carbon::now();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addMonths(1);
        $token->save();
        $user->save();

        return JsonResponse::on_success([
            'access_token' => $tokenResult->accessToken,
            'id' => $user->id,
            'first_name' => $user->first_name,
            'email' => $user->email,
            'picture' => $user->profile_image,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);

    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return JsonResponse::on_success();
    }

}
