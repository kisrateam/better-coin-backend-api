<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;

class MigrateController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('command'))
        {
            $command = $request->input('command');
            if ($command === 'migrate')
                return $this->migrate();
            else if ($command === 'rollback')
            {
                if ($request->has('step'))
                    return $this->rollback($request->input('step'));

                return $this->rollback();
            }
            else if ($command === 'reset')
            {
                return $this->reset();
            }
            else
                return response()->json('Command not found.', 500);
        }
        else {
            return $this->migrate();
        }
    }

    function migrate()
    {
        try{
            $migrate = Artisan::call('migrate');

            if ($migrate === 0)
                return response()->json('command success', 200);
            else
                return response()->json($migrate, 200);
        }
        catch (\Exception $exception)
        {
            return response()->json($exception->getMessage(), 500);
        }

    }

    function reset()
    {
        try{
            $migrate = Artisan::call('migrate:reset');
            if ($migrate === 0)
                return response()->json('command success', 200);
            else
                return response()->json($migrate, 200);
        }
        catch (\Exception $exception)
        {
            return response()->json($exception->getMessage(), 500);
        }
    }

    function rollback($step = null)
    {
        if (!is_null($step))
            $rollback = 'migrate:rollback --step='.$step;
        else
            $rollback = 'migrate:rollback';

        try{
            $migrate = Artisan::call($rollback);
            if ($migrate === 0)
                return response()->json('command success', 200);
            else
                return response()->json($migrate, 200);
        }
        catch (\Exception $exception)
        {
            return response()->json($exception->getMessage(), 500);
        }
    }
}
