<?php

namespace App\Http\Controllers;

use App\SocialUser;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class FacebookController extends Controller
{

    static $FACEBOOK_ACCESS_TOKEN_URI = "https://graph.facebook.com/v3.1/oauth/access_token";
    static $FACEBOOK_ACCESS_DATA_URI = "https://graph.facebook.com/v2.10/me";
    static $CLIENT_ID = 2081695412144802;
    static $REDIRECT_URI_PATH = "api/auth/facebook/oauth";
    static $CLIENT_SECRET = "8037860315a2ade0f4f1a0d02010c9f5";


    public function oauth(Request $request)
    {

        if ($request->has('code')) {

            $code = $request->input('code');

            $url = self::$FACEBOOK_ACCESS_TOKEN_URI."?client_id=".self::$CLIENT_ID."&redirect_uri=".url(self::$REDIRECT_URI_PATH)."&client_secret=".self::$CLIENT_SECRET."&code=".$code;

            $client = new Client();

            try {
                $response = $client->request('GET', $url, ['verify' => false])->getBody()->getContents();

                $response_json = json_decode($response);

                if (isset($response_json->access_token)){
                    $access_token = $response_json->access_token;

                    return $this->get_profile($access_token);

                }
                else {
                    return response()->json('Unauthorized', 401);
                }

            } catch (BadResponseException $exception) {
                return response()->json(["msg" => $exception->getMessage()], 401);
            }
        }
        else {
            return response()->json('Unauthorized', 401);
        }
    }

    function access_token(Request $request)
    {
        if ($request->has('access_token'))
        {
            $access_token = $request->input('access_token');
            return $this->get_profile($access_token);
        }
        else
        {
            return response()->json('Unauthorized', 401);
        }
    }

    function profile_with_token(Request $request)
    {
        if ($request->has('access_token'))
        {
            $access_token = $request->input('access_token');
            $user = Socialite::driver('facebook')->userFromToken($access_token);

            dd($user);
        }
        else
        {
            return response()->json('Unauthorized', 401);
        }
    }

    function get_profile($access_token)
    {

        $client = new Client();

        try
        {
            $fields = 'id,location,email,first_name,last_name,age_range,gender,birthday,picture.height(1000).width(1000)';

            $data_url = self::$FACEBOOK_ACCESS_DATA_URI.'?fields='.$fields.'&access_token='.$access_token;

            $data = $client->request('GET', $data_url, ['verify' => false])->getBody()->getContents();

            $data_json = json_decode($data);

//            dd($data_json);

            $id = $data_json->id;
            $location = isset($data_json->location)?$data_json->location->name:"";
            $email = isset($data_json->email)?$data_json->email:null;
            $first_name = $data_json->first_name;
            $last_name = $data_json->last_name;
            $age =  isset($data_json->age_range)?$data_json->age_range->min:null;
            $gender = isset($data_json->gender)?$data_json->gender:null;
            $birthday = isset($data_json->birthday)?Carbon::parse($data_json->birthday):null;
            $picture = base64_encode(file_get_contents($data_json->picture->data->url));

            if (!is_null($birthday)){
                $age = $birthday->age;
            }

            $response_data = [
                "social_id" => $id,
                "social_name" => 'facebook',
                "email" => $email,
                "first_name" => $first_name,
                "last_name" => $last_name,
                "age" => $age,
                "gender" => $gender,
                "location" => $location,
                "birthday" => $birthday->timestamp,
                "picture" => $picture
            ];

            $social_request = new Request();
            $social_request->merge($response_data);
            $auth = new AuthController();

            return $auth->social_login($social_request);
        }
        catch (BadResponseException $exception)
        {
            return response()->json(["msg" => $exception->getMessage()], 401);
        }

    }

}
