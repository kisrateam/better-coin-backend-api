<?php

namespace App\Http\Controllers;

use App\QuestHistory;
use Illuminate\Http\Request;

class QuestHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuestHistory  $questHistory
     * @return \Illuminate\Http\Response
     */
    public function show(QuestHistory $questHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestHistory  $questHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestHistory $questHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestHistory  $questHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestHistory $questHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestHistory  $questHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestHistory $questHistory)
    {
        //
    }
}
