<?php

namespace App\Http\Controllers;

use App\WeeklyReward;
use Illuminate\Http\Request;

class WeeklyRewardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeeklyReward  $weeklyReward
     * @return \Illuminate\Http\Response
     */
    public function show(WeeklyReward $weeklyReward)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeeklyReward  $weeklyReward
     * @return \Illuminate\Http\Response
     */
    public function edit(WeeklyReward $weeklyReward)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeeklyReward  $weeklyReward
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WeeklyReward $weeklyReward)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeeklyReward  $weeklyReward
     * @return \Illuminate\Http\Response
     */
    public function destroy(WeeklyReward $weeklyReward)
    {
        //
    }
}
