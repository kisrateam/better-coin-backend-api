<?php

namespace App\Http\Controllers;

use App\Http\JsonResponse;
use App\RewardSet;
use Illuminate\Http\Request;
use Validator;

class RewardSetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->permission_level < 1)
        {
            return JsonResponse::on_success(RewardSet::join('reward_items', 'reward_set.id', '=', 'reward_items.reward_set_id')
                ->where('used', true)
                ->paginate()
            );
        }

        return JsonResponse::on_success(RewardSet::join('reward_items', 'reward_set.id', '=', 'reward_items.reward_set_id')
            ->paginate()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'used' => 'required|boolean'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        // create user function
        $this->build($request->all());

        return JsonResponse::on_success();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RewardSet $rewardSet
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, RewardSet $rewardSet)
    {
        $reward_set["name"] = $rewardSet->name;
        if ($request->permission_level < 1)
            $reward_set["used"] = $rewardSet->used;

        $reward_items = $rewardSet->items;

        foreach ($reward_items as $reward_item)
        {
            if (($reward_item->reward_type) === 'item')
                $reward_items_detail[] = ["item" => $reward_item->item, "amount" => $reward_item->amount];
            else if ($reward_item->reward_type === 'coin')
            {
                $reward_items_detail[] = ["coin" => $reward_item->amount];
            }
        }
        $reward_set["items"] = $reward_items_detail;
        return JsonResponse::on_success($reward_set);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RewardSet $rewardSet
     * @return \Illuminate\Http\Response
     */
    public function edit(RewardSet $rewardSet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\RewardSet $rewardSet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RewardSet $rewardSet)
    {
        // create validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'used' => 'required|boolean'
        ]);

        // check input validate fails
        if($validator->fails())
            return JsonResponse::on_fails(JsonResponse::$MALFORMED, $validator->errors());

        $rewardSet->name = $request->input('name');
        $rewardSet->used = $request->input('used');

        $rewardSet->save();

        return JsonResponse::on_success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RewardSet $rewardSet
     * @return \Illuminate\Http\Response
     */
    public function destroy(RewardSet $rewardSet)
    {
        try
        {
            $reward_set_id = $rewardSet->id;
            $rewardSet->delete();

            $reward_items = RewardItemController::get($reward_set_id);

            foreach ($reward_items as $reward_item)
            {
                $reward_item->delete();
            }
        }
        catch (\Exception $exception)
        {
            return JsonResponse::on_fails(JsonResponse::$ERROR, $exception->getMessage());
        }

    }

    public function build($data)
    {
        $reward_set = new RewardSet([
            'name' => $data['name'],
            'used' => $data['used']
        ]);
        $reward_set->save();

        $reward_items = RewardItemController::build($data["items"], $reward_set->id);

        return JsonResponse::on_success([
            'reward_set' => $reward_set,
            'reward_items' => $reward_items
        ]);
    }
}
