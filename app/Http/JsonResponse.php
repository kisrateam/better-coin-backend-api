<?php
/**
 * Created by PhpStorm.
 * User: Pongp
 * Date: 10/16/2018
 * Time: 4:12 PM
 */

namespace App\Http;


class JsonResponse
{

    static $ERROR = [
        "code" => 500,
        "status" => "server error."
    ];

    static $MALFORMED = [
        "code" => 400,
        "status" => "request is malformed."
    ];

    static $ACCESS_FORBIDDEN = [
        "code" =>  403,
        "status" => "access forbidden."
    ];

    static $DATA_EXIST = [
        "code" => 409,
        "status" => "data exist."
    ];

    static $AUTHORIZED = [
        "code" => 401,
        "status" => 'unauthorized.'
    ];

    static $NOT_FOUND = [
        "code" => 404,
        "status" => 'not found.'
    ];

    static function on_success($data=[])
    {

        $json = new \stdClass();
        $json->code = 200;
        $json->status = 'ok';
        $json->result = $data;

        return response()->json($json, $json->code);

    }

    static function on_fails($error, $data = [])
    {
        $json = new \stdClass();
        $json->code = $error["code"];
        $json->status = 'error';
        $json->mes = $error["status"];
        $json->error = $data;

        return response()->json($json, $json->code);
    }

}
