<?php

namespace App\Http\Middleware;

use App\Http\JsonResponse;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if( $request->permission_level <1)
            return JsonResponse::on_fails(JsonResponse::$ACCESS_FORBIDDEN);

        return $next($request);
    }
}
