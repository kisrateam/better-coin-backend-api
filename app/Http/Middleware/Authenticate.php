<?php

namespace App\Http\Middleware;

use App\Http\JsonResponse;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Exceptions\HttpResponseException;

class Authenticate extends Middleware
{

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate($request, array $guards)
    {
        $request->request->add(['user_type'=> 'user']);

        if ($request->user('api'))
        {
            if ($request->user('api')->active === 'inactive')
                throw new HttpResponseException(JsonResponse::on_fails(JsonResponse::$AUTHORIZED));

            switch ($request->user('api')->group){
                case 'user':
                    $request->request->add(['permission_level'=> 0]);
                    break;
                case 'admin':
                    $request->request->add(['permission_level'=> 1]);
                    break;
                case 'superadmin':
                    $request->request->add(['permission_level'=> 9]);
                    break;
                default:
                    throw new HttpResponseException(JsonResponse::on_fails(JsonResponse::$AUTHORIZED));
            }

        }



        if (empty($guards)) {
            $guards = [null];
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        throw new HttpResponseException(JsonResponse::on_fails(JsonResponse::$AUTHORIZED));
    }

}
