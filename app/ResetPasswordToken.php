<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResetPasswordToken extends Model
{
    protected $fillable = [
        'email', 'token'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
