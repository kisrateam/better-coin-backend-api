<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardItem extends Model
{
    protected $fillable = ['reward_set_id', 'item_id', 'reward_type', 'amount'];

    protected  $hidden = ['updated_at'];

    public function reward_set()
    {
        return $this->belongsTo('App\RewardSet', 'reward_set_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }
}
