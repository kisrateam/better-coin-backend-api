<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardSet extends Model
{
    protected $fillable = ['name', 'used'];

    protected $hidden = ['updated_at'];

    public function items()
    {
        return $this->hasMany('App\RewardItem');
    }
}
