<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStatistic extends Model
{

    protected $fillable = [
        'user_id',
        'better_amount',
        'total_coin_amount',
        'total_distances',
        'total_points',
        'last_week_distances',
        'last_week_points',
        'last_week_coin_amount',
        'month_week_distances',
        'month_week_points',
        'month_week_coin_amount'
    ];

    protected $hidden = ['updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
