<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCoin extends Model
{
    protected $fillable = ['user_id', 'better_amount'];

    protected $hidden = ['updated_at', 'user_id', 'created_at', 'id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
