<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationEvent extends Model
{
    protected $fillable = [
        'name',
        'uri',
        'image',
        'image_cover',
        'description',
        'location',
        'total_donated',
        'received_donated',
        'received_better',
        'max_donated',
        'active',
        'started_at',
        'ended_at'
    ];

    protected $hidden = [
        //
    ];
}
