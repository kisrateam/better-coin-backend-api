<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserItem extends Model
{
    protected $fillable = ["user_id", "item_id"];

    protected $hidden = ['updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function detail()
    {
        return $this->belongsTo('App\Item', 'item_id');
    }
}
