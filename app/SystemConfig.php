<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemConfig extends Model
{

    static $DEFAULT_BETTER_RATE = 1;

    protected $fillable = ['step_exceed', 'ad_watch_exceed', 'user_item_capacity', 'better_rate', 'friend_coin_limit', 'friend_coin_amount', 'used'];

    protected $hidden = [];
}
