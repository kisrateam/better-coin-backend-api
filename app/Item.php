<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ["name", "ability", "coin", "ability_period", "ability_capacity", "ability_multiple", "duration", "better_price", "money_price"];

    protected $hidden = ['created_at', 'updated_at'];

    public function user_item()
    {
        return $this->hasMany('App\UserItem');
    }
}
