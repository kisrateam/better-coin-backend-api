<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'first_name' => $faker->firstName('male'),
        'last_name' => $faker->lastName,
        'gender' => 'male',
        'age' => rand(13,40),
        'profile_image' => null,
        'verified' => 'unverify',
        'active' => 'active',
        'last_login' => \Carbon\Carbon::now(),
        'remember_token' => str_random(10),
    ];
});
