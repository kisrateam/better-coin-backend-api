<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendCoinGiftHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friend_coin_gift_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('history_id');
            $table->unsignedInteger('from_user_id');
            $table->unsignedInteger('message_id');
            $table->unsignedInteger('to_user_id');
            $table->timestamps();

            $table->foreign('history_id')->references('id')->on('user_histories');
            $table->foreign('from_user_id')->references('id')->on('users');
            $table->foreign('to_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('friend_coin_gift_histories');
    }
}
