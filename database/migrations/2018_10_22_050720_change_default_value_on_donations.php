<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultValueOnDonations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donation_events', function (Blueprint $table) {
            DB::statement('ALTER TABLE donation_events MODIFY total_donated DECIMAL(10, 2) UNSIGNED DEFAULT 0 NOT NULL ;');
            DB::statement('ALTER TABLE donation_events MODIFY received_donated DECIMAL(10, 2) UNSIGNED DEFAULT 0 NOT NULL ;');
            DB::statement('ALTER TABLE donation_events MODIFY received_better DECIMAL(10, 2) UNSIGNED DEFAULT 0 NOT NULL ;');
            DB::statement('ALTER TABLE donation_events MODIFY max_donated DECIMAL(10, 2) UNSIGNED NULL COMMENT "max donate each user" ;');
            DB::statement('ALTER TABLE donation_events MODIFY active BOOLEAN DEFAULT FALSE NOT NULL ;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donation_events', function (Blueprint $table) {
            DB::statement('ALTER TABLE donation_events MODIFY total_donated DECIMAL(10, 2) UNSIGNED NOT NULL ;');
            DB::statement('ALTER TABLE donation_events MODIFY received_donated DECIMAL(10, 2) UNSIGNED NOT NULL ;');
            DB::statement('ALTER TABLE donation_events MODIFY received_bette DECIMAL(10, 2) UNSIGNED NOT NULL ;');
            DB::statement('ALTER TABLE donation_events MODIFY max_donated DECIMAL(10, 2) UNSIGNED NOT NULL COMMENT "max donate each user" ;');
            DB::statement('ALTER TABLE donation_events MODIFY active BOOLEAN NOT NULL ;');
        });
    }
}
