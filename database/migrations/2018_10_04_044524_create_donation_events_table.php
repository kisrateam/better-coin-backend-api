<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donation_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('uri');
            $table->text('image')->nullable();
            $table->text('image_cover')->nullable();
            $table->text('description')->nullable();
            $table->string('location');
            $table->unsignedDecimal('total_donated', 10, 2);
            $table->unsignedDecimal('received_donated', 10, 2);
            $table->unsignedDecimal('received_better', 10, 2);
            $table->unsignedDecimal('max_donated', 10, 2)->comment('max donate each user');
            $table->boolean('active');
            $table->timestamp('started_at');
            $table->timestamp('ended_at')->default(\Carbon\Carbon::now());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donation_events');
    }
}
