<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainQuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_quests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('reward_set_id');
            $table->enum('quest_type', ['invite', 'login', 'event']);
            $table->string('name');
            $table->text('description')->nullable();
            $table->unsignedDecimal('points', 10, 2);
            $table->boolean('used');
            $table->timestamp('started_at');
            $table->timestamp('ended_at')->default(\Carbon\Carbon::now());
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('reward_set_id')->references('id')->on('reward_sets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_quests');
    }
}
