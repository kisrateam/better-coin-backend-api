<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('to_user_id');
            $table->unsignedInteger('item_id')->nullable();
            $table->enum('message_type', ['message', 'gift', 'friend_coin']);
            $table->unsignedInteger('from_user_id');
            $table->unsignedDecimal('friend_coin', 10, 2)->default(0);
            $table->text('message');
            $table->boolean('read');
            $table->boolean('received');
            $table->timestamps();

            $table->foreign('to_user_id')->references('id')->on('users');
            $table->foreign('from_user_id')->references('id')->on('users');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_messages');
    }
}
