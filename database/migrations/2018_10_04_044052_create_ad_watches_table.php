<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdWatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_watches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reward_set_id')->nullable();
            $table->string('name');
            $table->string('token');
            $table->text('contents');
            $table->boolean('used');
            $table->unsignedDecimal('points', 10, 2);
            $table->boolean('skippable');

            $table->timestamps();

            $table->foreign('reward_set_id')->references('id')->on('reward_sets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_watches');
    }
}
