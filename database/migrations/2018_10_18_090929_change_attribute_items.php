<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAttributeItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            DB::statement('ALTER TABLE items MODIFY ability TEXT NULL ;');
            DB::statement('ALTER TABLE items MODIFY coin DECIMAL(10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE items MODIFY better_price DECIMAL(10,2) UNSIGNED NULL ;');
            DB::statement('ALTER TABLE items MODIFY money_price DECIMAL(10,2) UNSIGNED NULL ;');
            DB::statement('ALTER TABLE items MODIFY duration DECIMAL(10,2) UNSIGNED NULL COMMENT "minutes";');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            DB::statement('ALTER TABLE items MODIFY ability TEXT NOT NULL ;');
            DB::statement('ALTER TABLE items MODIFY coin DECIMAL(10,2) UNSIGNED NOT NULL ;');
            DB::statement('ALTER TABLE items MODIFY better_price DECIMAL(10,2) UNSIGNED NOT NULL ;');
            DB::statement('ALTER TABLE items MODIFY money_price DECIMAL(10,2) UNSIGNED NOT NULL ;');
            DB::statement('ALTER TABLE items MODIFY duration DECIMAL(10,2) UNSIGNED NOT NULL COMMENT "minutes";');

            DB::statement("COMMENT ON COLUMN items.duration IS 'minutes';");
        });
    }
}
