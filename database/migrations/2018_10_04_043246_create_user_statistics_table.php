<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedDecimal('total_coin_amount', 10, 2);
            $table->unsignedDecimal('total_distances', 10, 2);
            $table->unsignedDecimal('total_points', 10, 2);
            $table->unsignedDecimal('last_week_coin_amount', 10, 2);
            $table->unsignedDecimal('last_week_distances', 10, 2);
            $table->unsignedDecimal('last_week_points', 10, 2);
            $table->unsignedDecimal('last_month_coin_amount', 10, 2);
            $table->unsignedDecimal('last_month_distances', 10, 2);
            $table->unsignedDecimal('last_month_points', 10, 2);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_statistics');
    }
}
