<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeUsersFieldCanNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::statement('ALTER TABLE users MODIFY last_name VARCHAR(255)  NULL ;');
            DB::statement('ALTER TABLE users MODIFY age INT(11)  NULL ;');
            DB::statement('ALTER TABLE users MODIFY profile_image TEXT  NULL ;');

            DB::statement('ALTER TABLE users MODIFY gender ENUM("male","female") NULL ;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            DB::statement('ALTER TABLE users MODIFY last_name VARCHAR(255) NOT NULL ;');
            DB::statement('UPDATE  users SET age =  0;');
            DB::statement('ALTER TABLE users MODIFY age INT(11) NOT NULL DEFAULT 0;');
            DB::statement('UPDATE  users SET profile_image =  "";');
            DB::statement('ALTER TABLE users MODIFY profile_image TEXT NOT NULL ;');
            DB::statement('UPDATE  users SET gender =  "male";');
            DB::statement('ALTER TABLE users MODIFY gender ENUM("male","female") NOT NULL ;');
        });
    }
}
