<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('ability');
            $table->unsignedDecimal('coin', 10, 2);
            $table->unsignedDecimal('ability_period', 10, 2)->default(0)->comment('minutes');
            $table->unsignedInteger('ability_capacity')->default(0)->comment('amount of item');
            $table->unsignedInteger('ability_multiple')->default(0)->comment('multiple of coins');
            $table->text('image');
            $table->unsignedDecimal('better_price', 10, 2);
            $table->unsignedDecimal('money_price', 10, 2);
            $table->unsignedDecimal('duration', 10, 2)->comment('minutes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
