<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBluffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bluffs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('capacity')->default(0);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('multiple')->default(0);
            $table->boolean('active');
            $table->timestamp('started_at');
            $table->timestamp('ended_at' )->default(\Carbon\Carbon::now());
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_bluffs');
    }
}
