<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeklyRankingHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weekly_ranking_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('history_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('rank');
            $table->timestamp('ranking_dated_at');
            $table->timestamps();

            $table->foreign('history_id')->references('id')->on('user_histories');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weekly_ranking_histories');
    }
}
