<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('step_exceed');
            $table->unsignedInteger('ad_watch_exceed');
            $table->unsignedInteger('user_item_capacity');
            $table->unsignedInteger('better_rate');
            $table->unsignedInteger('friend_coin_limit');
            $table->unsignedDecimal('friend_coin_amount', 10 , 2);
            $table->boolean('used');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_configs');
    }
}
