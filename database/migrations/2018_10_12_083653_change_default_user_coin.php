<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultUserCoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_coins', function (Blueprint $table) {
            DB::statement('ALTER TABLE user_coins MODIFY better_amount DECIMAL (10,2) UNSIGNED DEFAULT 0;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_coins', function (Blueprint $table) {
            DB::statement('ALTER TABLE user_coins MODIFY better_amount DECIMAL (10,2) UNSIGNED NOT NULL;');
        });
    }
}
