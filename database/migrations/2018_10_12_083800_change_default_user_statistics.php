<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultUserStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_statistics', function (Blueprint $table) {
            DB::statement('ALTER TABLE user_statistics MODIFY total_coin_amount  DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY total_distances DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY total_points DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_week_coin_amount DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_week_distances DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_week_points DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_month_coin_amount DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_month_distances DECIMAL (10,2) UNSIGNED DEFAULT 0;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_month_points DECIMAL (10,2) UNSIGNED DEFAULT 0;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_statistics', function (Blueprint $table) {
            DB::statement('ALTER TABLE user_statistics MODIFY total_coin_amount  DECIMAL (10,2) NOT NULL ;');
            DB::statement('ALTER TABLE user_statistics MODIFY total_distances DECIMAL (10,2) NOT NULL;');
            DB::statement('ALTER TABLE user_statistics MODIFY total_points DECIMAL (10,2) NOT NULL;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_week_coin_amount DECIMAL (10,2) NOT NULL;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_week_distances DECIMAL (10,2) NOT NULL;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_week_points DECIMAL (10,2) NOT NULL;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_month_coin_amount DECIMAL (10,2) NOT NULL;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_month_distances DECIMAL (10,2) NOT NULL;');
            DB::statement('ALTER TABLE user_statistics MODIFY last_month_points DECIMAL (10,2) NOT NULL;');
        });
    }
}
