<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdWatchHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_watch_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('history_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('ad_id');
            $table->timestamps();

            $table->foreign('history_id')->references('id')->on('user_histories');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('ad_id')->references('id')->on('ad_watches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_watch_histories');
    }
}
