<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsedDefaultCanNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_items', function (Blueprint $table) {
            DB::statement('ALTER TABLE user_items MODIFY used BOOLEAN DEFAULT FALSE NOT NULL ;');
            DB::statement('ALTER TABLE user_items MODIFY expired BOOLEAN DEFAULT FALSE NOT NULL ;');
            DB::statement('ALTER TABLE user_items MODIFY used_at TIMESTAMP NULL ;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_items', function (Blueprint $table) {
            DB::statement('ALTER TABLE user_items MODIFY used BOOLEAN  NOT NULL ;');
            DB::statement('ALTER TABLE user_items MODIFY expired BOOLEAN  NOT NULL ;');
            DB::statement('ALTER TABLE user_items MODIFY used_at TIMESTAMP NOT NULL CURRENT_TIMESTAMP ;');
        });
    }
}
