<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventQuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_quests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('quest_id');
            $table->text('image')->nullable();
            $table->string('location');
            $table->unsignedDecimal('ability_period', 10, 2)->default(0)->comment('minutes');
            $table->unsignedInteger('ability_capacity')->default(0)->comment('amount of item');
            $table->unsignedInteger('ability_multiple')->default(0)->comment('multiple of coins');
            $table->timestamp('started_at');
            $table->timestamp('ended_at')->default(\Carbon\Carbon::now());
            $table->unsignedDecimal('duration', 10, 2)->default(0)->comment('quest mission (minutes)');
            $table->unsignedInteger('steps')->default(0)->comment('quest mission (steps count)');
            $table->timestamps();

            $table->foreign('quest_id')->references('id')->on('main_quests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_quests');
    }
}
