<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reward_set_id');
            $table->unsignedInteger('item_id')->nullable();
            $table->enum('reward_type', ['item', 'coin']);
            $table->unsignedDecimal('amount',10, 2);
            $table->timestamps();

            $table->foreign('reward_set_id')->references('id')->on('reward_sets');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_items');
    }
}
