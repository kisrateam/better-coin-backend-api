<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monthly_rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reward_set_id');
            $table->unsignedInteger('rank');
            $table->boolean('used');
            $table->timestamps();

            $table->foreign('reward_set_id')->references('id')->on('reward_sets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_rewards');
    }
}
