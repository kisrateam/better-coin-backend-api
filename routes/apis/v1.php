<?php
/**
 * Created by PhpStorm.
 * User: Pongp
 * Date: 10/17/2018
 * Time: 4:36 PM
 */


// signup
Route::resource('signup', 'UserController', ['only' => 'store']);
// reset password
Route::group([
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::resource('res', 'ResetPasswordTokenController');
    Route::get('find/{token}', 'ResetPasswordTokenController@find');
    Route::post('reset', 'ResetPasswordTokenController@reset');
});
// login
Route::group([
    'prefix' => 'auth'
], function () {
    // normal login
    Route::post('login', 'AuthController@login');
    Route::post('admin/login', 'AuthController@admin_login');
    // line login
    Route::group([
        'prefix' => 'line'
    ], function () {
        Route::get('oauth', 'LineController@oauth');
        Route::get('access_token', 'LineController@access_token');
    });
    // facebook login
    Route::group([
        'prefix' => 'facebook'
    ], function () {
        Route::get('oauth', 'FacebookController@oauth');
        Route::get('access_token', 'FacebookController@access_token');
        Route::get('profile_with_token', 'FacebookController@profile_with_token');
    });
    // google login
    Route::group([
        'prefix' => 'google'
    ], function () {
        Route::get('oauth', 'GoogleController@oauth');
        Route::get('access_token', 'GoogleController@access_token');
    });
    // logout
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
    });
});
// CRUD user for user
Route::resource('user', 'UserController')->middleware('auth:api');
// user profile
Route::group([
    'prefix' => 'profile',
    'middleware' => 'auth:api'
], function() {
    Route::get('test', 'FirebaseController@test');
    // upload profile image
    Route::post('image', 'UserController@upload_profile_image');
    // delete profile image
    Route::delete('image', 'UserController@upload_profile_image');
    // get user coin
    Route::get('coin', 'UserController@coin');
    // get user statistic
    Route::get('statistic', 'UserController@statistic');
    // get user bluff
    Route::get('bluff', 'UserController@bluff');
    // get user item
    Route::get('item', 'UserController@item');
});
Route::group([
    'prefix' => 'items'
], function() {
    Route::resource('show', 'ItemController', ['only'=> 'index', 'show'])->middleware('auth:api');
    Route::resource('rewardset', 'RewardSetController', ['only'=> 'index', 'show'])->middleware('auth:api');
    Route::resource('rewarditem', 'RewardSetController', ['only'=> 'show'])->middleware('auth:api');
});

Route::resource('news', 'NewsController', ['only' => 'index, show'])->middleware('auth:api');
Route::group([
    'prefix' => 'firebase'
], function() {
    Route::get('index', 'FirebaseController@index');
    Route::post('walk', 'FirebaseController@walk');
    Route::get('walk_variation', 'FirebaseController@coin_variation');
});
// admin
Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth:api', 'admin']
], function (){
    // CRUD user
    Route::resource('user', 'UserController');
    Route::resource('item', 'ItemController');
    Route::resource('news', 'NewsController');
    Route::resource('system', 'SystemConfigController');
    Route::resource('donations', 'NewsController');
    Route::resource('rewardset', 'RewardSetController');
    Route::resource('rewarditem', 'RewardItemController');
});
// superadmin
Route::group([
    'prefix' => 'superadmin'
], function() {
    // migrate database
    Route::get('migrate', 'MigrateController@index');
    // install passport for oauth2
    Route::get('passport:install', function () {
        DB::table('oauth_clients')->insert(
            ['id' => 1, 'name' => 'Laravel Personal Access Client',
                'secret' => 'tRVflnc0YOJlEHBFl0PGpgKwstjSDSkmIF5JhLKi',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0
            ]
        );
        DB::table('oauth_clients')->insert(
            ['id' => 2, 'name' => 'Laravel Password Grant Client',
                'secret' => 'RW9OM9WohshdCC1iCqnNPAZPzRBh8iiPUmlQGhMM',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0
            ]
        );
        DB::table('oauth_personal_access_clients')->insert(
            ['id' => 1, 'client_id' => 1]
        );
        return response()->json('passport install success', 200);
    });
});
