<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => '2081695412144802',
        'client_secret' => '8037860315a2ade0f4f1a0d02010c9f5',
        'redirect' => 'https://bettercoin.kisrateam.com/api/auth/facebook/oauth',
    ],
    'google' => [
        'client_id' => '537020008116-76asqk21h146suv2np0v3890tpvh0nng.apps.googleusercontent.com',
        'client_secret' => 'cIxRIBplnP0h13mwTKTF6DVH',
        'redirect' => 'https://bettercoin.kisrateam.com/api/v1/auth/google/oauth',
    ],

];
