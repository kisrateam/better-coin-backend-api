## Installing Guide

-   composer update
-   php artisan migrate
-   php artisan passport:install

### Get Facebook login

-   GET URL : https://www.facebook.com/v2.10/dialog/oauth?response_type=code&client_id=2081695412144802&redirect_uri=https://bettercoin.kisrateam.com/api/auth/facebook/oauth&scope=public_profile%20user_friends%20email%20user_birthday%20user_gender%20user_location%20user_hometown%20user_age_range

### Get Line login

-   GET URL : https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=1613841815&redirect_uri=https://bettercoin.kisrateam.com/api/auth/line/oauth&state=1sdsa345abcde&scope=openid%20profile%20email

### Get Google login

-   GET URL : https://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/plus.profile.emails.read&access_type=offline&include_granted_scopes=true&state=state_parameter_passthrough_value&redirect_uri=https://bettercoin.kisrateam.com/api/auth/google/oauth&response_type=code&client_id=537020008116-76asqk21h146suv2np0v3890tpvh0nng.apps.googleusercontent.com
